﻿/*добавление нового производителя и автомобиля в каталог производителей*/
insert into manufacturer (name)
values ('Nisan');
insert into car (model, speed, color, id_manufacturer)
select 'Teana', 200, 'Зелёный сад', id_manufacturer from manufacturer
where name = 'Nisan';
insert into car_catalog_manufacturer(price, amount, id_car)
select 200000, 2, id_car from car
where model = 'Teana' and speed = 200 and color = 'Зелёный сад';

