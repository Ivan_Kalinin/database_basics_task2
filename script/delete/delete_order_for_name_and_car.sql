﻿/*удаление записи из списка заказов по имени покупателя, названию автомобиля, и цвету автомобиля*/
DELETE FROM order_list
WHERE id_customer in(
Select id_customer from customer where first_name = 'Семенова' and second_name = 'Елена' and third_name = 'Викторовна')
and id_car_catalog in(
Select id_car_catalog from car_catalog where id_car in(
Select id_car from car where model = 'Mustang' and color='Ярко-зелёный'))