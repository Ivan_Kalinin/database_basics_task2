﻿/*Запрос на получение подробной информации о сделках с покупателями*/

select status, f_n_customer, s_n_customer, t_n_customer,
first_name, second_name, third_name, price, model, speed, color,
name as manufacturer_name, amount
from 
(select * from 
(select * from 
(select * from
(select * from
(select id_order_list, order_list.id_customer,
id_sellers, id_car_catalog, status, first_name as f_n_customer,
second_name as s_n_customer,
third_name as t_n_customer
from order_list inner join customer on order_list.id_customer = customer.id_customer)
as a 
inner join sellers on a.id_sellers=sellers.id_sellers) 
as b 
inner join car_catalog on b.id_car_catalog = car_catalog.id_car_catalog) 
as c 
inner join car on c.id_car = car.id_car) 
as d
inner join manufacturer on d.id_manufacturer = manufacturer.id_manufacturer) 
as e