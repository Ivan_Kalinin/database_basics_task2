﻿CREATE INDEX car_catalog_index ON car_catalog USING btree(price,amount);
CREATE INDEX car_catalog_manufacturer_index ON car_catalog_manufacturer USING btree(price, amount);
CREATE INDEX order_list_index ON order_list USING btree(id_order_list, id_customer, id_car_catalog, id_sellers);