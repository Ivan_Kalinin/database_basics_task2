CREATE TABLE "customer" (
	"id_customer" serial NOT NULL,
	"first_name" varchar NOT NULL,
	"second_name" varchar NOT NULL,
	"third_name" varchar NOT NULL,
	CONSTRAINT customer_pk PRIMARY KEY ("id_customer")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "order_list" (
	"id_order_list" serial NOT NULL,
	"id_customer" integer NOT NULL,
	"id_car_catalog" integer NOT NULL,
	"id_sellers" integer NOT NULL,
	"status" varchar NOT NULL,
	CONSTRAINT order_list_pk PRIMARY KEY ("id_order_list")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "car_catalog" (
	"id_car_catalog" serial NOT NULL,
	"price" DECIMAL(10,2) NOT NULL,
	"amount" integer NOT NULL,
	"id_car" integer NOT NULL UNIQUE,
	CONSTRAINT car_catalog_pk PRIMARY KEY ("id_car_catalog")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "car" (
	"id_car" serial NOT NULL,
	"model" varchar NOT NULL,
	"speed" integer NOT NULL,
	"color" varchar NOT NULL,
	"id_manufacturer" integer NOT NULL,
	CONSTRAINT car_pk PRIMARY KEY ("id_car")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "car_catalog_manufacturer" (
	"id_car_catalog_manufacturer" serial NOT NULL,
	"price" DECIMAL(10,2) NOT NULL,
	"amount" integer NOT NULL,
	"id_car" integer NOT NULL UNIQUE,
	CONSTRAINT car_catalog_manufacturer_pk PRIMARY KEY ("id_car_catalog_manufacturer")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "manufacturer" (
	"id_manufacturer" serial NOT NULL,
	"name" varchar NOT NULL,
	CONSTRAINT manufacturer_pk PRIMARY KEY ("id_manufacturer")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "order_list_from_manufacturer" (
	"id_order_list_from_manufacturer" serial NOT NULL,
	"id_car_catalog_manufacturer" integer NOT NULL,
	"id_sellers" integer NOT NULL,
	"status" varchar NOT NULL,
	CONSTRAINT order_list_from_manufacturer_pk PRIMARY KEY ("id_order_list_from_manufacturer")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "sellers" (
	"id_sellers" serial NOT NULL,
	"first_name" varchar NOT NULL,
	"second_name" varchar NOT NULL,
	"third_name" varchar NOT NULL,
	CONSTRAINT sellers_pk PRIMARY KEY ("id_sellers")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "order_list" ADD CONSTRAINT "order_list_fk0" FOREIGN KEY ("id_customer") REFERENCES "customer"("id_customer");
ALTER TABLE "order_list" ADD CONSTRAINT "order_list_fk1" FOREIGN KEY ("id_car_catalog") REFERENCES "car_catalog"("id_car_catalog");
ALTER TABLE "order_list" ADD CONSTRAINT "order_list_fk2" FOREIGN KEY ("id_sellers") REFERENCES "sellers"("id_sellers");

ALTER TABLE "car_catalog" ADD CONSTRAINT "car_catalog_fk0" FOREIGN KEY ("id_car") REFERENCES "car"("id_car");

ALTER TABLE "car" ADD CONSTRAINT "car_fk0" FOREIGN KEY ("id_manufacturer") REFERENCES "manufacturer"("id_manufacturer");

ALTER TABLE "car_catalog_manufacturer" ADD CONSTRAINT "car_catalog_manufacturer_fk0" FOREIGN KEY ("id_car") REFERENCES "car"("id_car");


ALTER TABLE "order_list_from_manufacturer" ADD CONSTRAINT "order_list_from_manufacturer_fk0" FOREIGN KEY ("id_car_catalog_manufacturer") REFERENCES "car_catalog_manufacturer"("id_car_catalog_manufacturer");
ALTER TABLE "order_list_from_manufacturer" ADD CONSTRAINT "order_list_from_manufacturer_fk1" FOREIGN KEY ("id_sellers") REFERENCES "sellers"("id_sellers");


